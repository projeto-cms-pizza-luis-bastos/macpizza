import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'

export default () => {
    return (
        <Footer>
            <Container>
                <FooterInfo>
                    <Row>
                        <Col md={5}>
                            <div className="title">Sobre Nós</div>
                            <div className="aboutUs">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="title">Onde nos encontrar!</div>
                            <div className="menu">
                                <div className="text">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.</p>
                                </div>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="title">Redes Sociais</div>
                            <div className="address">
                                <div className="block-23 mb-3">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.</p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </FooterInfo>
                <Row>
                    <FooterCopy>
                        Development being made by Luis Bastos
                </FooterCopy>
                </Row>
            </Container>
        </Footer>
    )
}



const Footer = styled.div`
    background: pink;
    padding: 10px 0;
    color: black;
    border-top: 2px solid  black;
    position: absolute;
    bottom: 0;
    width: 100%;


`

const FooterInfo = styled.div`
    .title{
        font-size: 20px;
        font-weight: 600;
        padding: 10px 0;
        border-bottom: thin solid  black;
        margin-bottom: 10px;
        color: black;
    }
`

const FooterCopy = styled.div`
    width: 100%;
    padding: 10px;
    text-align: center;
`

