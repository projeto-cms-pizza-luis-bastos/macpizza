import React from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import styled from 'styled-components'
import { FaUtensils } from 'react-icons/fa'
import { NavLink } from 'react-router-dom'

export default (props) => {

    const menu = [
        {
            title: "Home",
            link: ''
        },
        {
            title: "Sobre",
            link: 'sobre'
        },
        {
            title: "Produtos",
            link: 'produtos'
        },
        {
            title: "Serviços",
            link: 'servicos'
        },
        {
            title: "Contato",
            link: 'contato'
        }

    ]

    return (
        <Header>
            <Container>
                <Navbar expand="lg" variant="dark">
                        <Logo>
                            <FaUtensils />MAC PIZZA
                            <br />
                        </Logo>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            {menu.map((item, i) => (
                                <NavLink exact={true} to={item.link} key={i}>
                                    <Nav.Link as="div">{item.title}</Nav.Link>
                                </NavLink>
                            ))}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
        </Header >
    )
}



const Header = styled.div`

    

    background-color: pink;
    font-family: Poppins , sans-serif;
    a{
        text-decoration: none;
    }

    .nav-link{
        
        :hover{
            color: green !important;
            font-weight: 500;
            text-decoration: none;
        }
    }
`

const Logo = styled.div`
    font-size: 20px;
    font-fontWeight: 600;
    margin:0;
    font-family: Poppins , sans-serif;

    svg{
        color: green;
        margin: -5px 1px;
    }

    span{
        color:  #fac564;
        margin:0;
        font-size: 12px;
        text-transform: uppercase;
        display: block;
        text-align: center;
        letter-spacing: 4px;
    }

`

