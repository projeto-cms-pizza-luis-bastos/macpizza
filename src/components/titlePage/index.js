import React from 'react'
import styled from 'styled-components'
import { Container } from 'react-bootstrap'
export default ({ title, sub }) => {
    return (
        <ContainerTitle>
            <Container>
                <Title>{title}</Title>
                <Sub>{sub}</Sub>
            </Container>
        </ContainerTitle>
    )
}



const ContainerTitle = styled.div`
    background: red;
    padding: 10px;
    font-family: 'Josefin Sans', sans-serif;

`
const Title = styled.div`
    color: black;
    font-size: 30px;
    font-weight: 600;
`
const Sub = styled.div`
   color: black;
   font-size: 20px;
   font-family: 'Josefin Sans', sans-serif;
`
