import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'

import TitlePage from '../../../components/titlePage'

export default () => {
    return (
        <Service>
            <TitlePage title="Serviços" sub="Obtenha informações de nossos serviços." />
            <Container>
                <Row>
                    <ServiceItem>1</ServiceItem>
                    <ServiceItem>2</ServiceItem>
                    <ServiceItem>3</ServiceItem>
                </Row>
            </Container>

        </Service>
    )
}


const Service = styled.div`
    display:block;
    height: 500px;
`


const ServiceItem = styled(Col)`
    background: pink;
    height: 200px;
    margin: 10px;
    width: 20%;
`


