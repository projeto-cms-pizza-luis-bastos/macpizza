import React from 'react'
import styled from 'styled-components'

import TitlePage from '../../../components/titlePage'
export default () => {
    return (
        <Product>
            <TitlePage title="Produtos" sub="Conheça nossa Lista de Produtos" />
        </Product>
    )
}


const Product = styled.div`
    display:block;
    height: 500px;

    .tab-content{
        background: #eee !important;
    }
`
