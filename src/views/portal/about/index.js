import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'


export default () => {
    return (
        <About>
            <TitlePage title="Sobre" sub="Nossa história é muito longa!" />


            <Description>
                <Container>
                </Container>
            </Description>
            <Collaborators>
                <Container>
                    <Row>
                        <BoxItem>1</BoxItem>
                        <BoxItem>2</BoxItem>
                        <BoxItem>3</BoxItem>
                        <BoxItem>4</BoxItem>
                    </Row>
                </Container>

            </Collaborators>
        </About>
    )
}


const About = styled.div`
    display:block;
`
const Description = styled.div`
    height: 200px;
    background: green;
    width: 100%;
    display:flex;
`
const Collaborators = styled.div`

    min-height: 200px;
    background: yellow;
    width: 100%;
    padding: 20px 0;
`
const BoxItem = styled(Col)`
    background: pink;
    height: 200px;
    margin: 10px;
    width: 20%;
`

